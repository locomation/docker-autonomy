################################################################################
# @file Dockerfile
# @author Tekin Mericli <tekin@locomation.ai>
# @brief Dockerfile locomation base image for Locomation perception development
# @date 2018-10-18
#
# @copyright
# Copyright (c) 2019 Locomation, Inc.\n
# All Rights Reserved.\n
# CONFIDENTIAL and PROPRIETARY\n
# @par
#
# @internal
# @par History
# * 2018-10-18 Tekin Mericli <tekin@locomation.ai> created file.
# @par Reviews
# * 2018-XX-XX John Doe
################################################################################
ARG from=locomation/base:18.04

FROM ${from}

LABEL maintainer="Cetin Mericli cetin@locomation.ai" 

USER root

RUN /bin/bash -c "echo 'deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main' > /etc/apt/sources.list.d/ros-latest.list"
RUN /bin/bash -c "apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116"

# ROS Melodic 
RUN apt-get update && \
    apt-get install -y --fix-missing --no-install-recommends \
	ros-melodic-desktop-full \
	ros-melodic-joystick-drivers \
	ros-melodic-joy-listener \
	ros-melodic-joy \
	python-tornado \
	python-bson \
	python-rosinstall-generator \
	python-wstool \
	python-rosinstall \
&& rm -rf /var/lib/apt/lists/*

RUN apt-get update && \ 
apt-get install -y --fix-missing --no-install-recommends build-essential \
&& rm -rf /var/lib/apt/lists/*

RUN updatedb
RUN rosdep init

USER $CUSTOM_USER

RUN rosdep update

RUN /bin/bash -c "echo 'source /opt/ros/melodic/setup.bash' >> ~/.bashrc"
RUN /bin/bash -c "source ~/.bashrc"

RUN cd ${HOME}

