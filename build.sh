#!/bin/bash
################################################################################
# @file build.sh
# @author Tekin Mericli <tekin@locomation.ai>
# @brief Build locomation-dev image
# @date 2018-10-18
#
# @copyright
# Copyright (c) 2018 Locomation, Inc.\n
# All Rights Reserved.\n
# CONFIDENTIAL and PROPRIETARY\n
# @par
#
# @internal
# @par History
# * 2018-10-18 Tekin Mericli <tekin@locomation.ai> created file.
# @par Reviews
# * 2018-XX-XX John Doe
################################################################################

echo "detected host UID=$UID"

docker build $1 --build-arg CUSTOM_UID=$UID -t locomation/locomation-dev .

